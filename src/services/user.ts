import type User from "@/types/User";
import http from "./axios";
function getUsers() {
  return http.get("/Users");
}

function saveUser(User: User) {
  return http.post("/Users", User);
}

function updateUser(id: number, User: User) {
  return http.patch(`/Users/${id}`, User);
}

function deleteUser(id: number) {
  return http.delete(`/Users/${id}`);
}

export default { getUsers, saveUser, updateUser, deleteUser };
