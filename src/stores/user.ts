import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import UserService from "@/services/user";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const Users = ref<User[]>([]);
  const editedUser = ref<User>({ login: "", name: "", password: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedUser.value = { login: "", name: "", password: "" };
    }
  });

  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await UserService.getUsers();
      Users.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await UserService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await UserService.saveUser(editedUser.value);
      }

      dialog.value = false;
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึก User ได้");
    }
    loadingStore.isLoading = false;
  }

  function editUser(User: User) {
    loadingStore.isLoading = true;
    editedUser.value = JSON.parse(JSON.stringify(User));
    dialog.value = true;
    loadingStore.isLoading = false;
  }

  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await UserService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ User ได้");
    }
    loadingStore.isLoading = false;
  }

  return {
    Users,
    getUsers,
    dialog,
    editedUser,
    saveUser,
    editUser,
    deleteUser,
  };
});
